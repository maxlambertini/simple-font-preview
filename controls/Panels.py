import wx
import wx.lib.gizmos as gizmos
import wx.lib.agw.customtreectrl as customtreectrl
import wx.lib.newevent
import os
from PIL import Image, ImageDraw, ImageFont
from controls.Fonts import FontData
from controls.Structs import Preferences
import glob

class ImportFontFromDirectoryDialog (wx.Dialog):
    def __init__ (self, *a, **k):
        wx.Panel.__init__(self,*a,**k)
        self.path = "."
        if "path" in k :
            self.path = k['path']
        self.SetTitle("Import fonts from path")
        self.create_controls()

    def SetPath (self, path):
        self.list_directory.SetPath(path)

    def create_controls(self):
        sizer = wx.GridBagSizer(8,8)
        sizer.Add(wx.StaticText(self, -1, label="Directory containing fonts", style=wx.ALIGN_RIGHT)      ,pos=(0,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Recurse subdirs?",            style=wx.ALIGN_RIGHT)      ,pos=(1,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Add or replace to group",    style=wx.ALIGN_RIGHT)      ,pos=(2,0), flag=wx.ALL | wx.EXPAND, border=8)
        
        self.list_directory = wx.GenericDirCtrl(self,-1, size=(500,300), style=wx.DIRCTRL_DIR_ONLY | wx.DIRCTRL_3D_INTERNAL)
        self.list_directory.SetPath(self.path)
        self.chk_recursive  = wx.CheckBox(self, -1, "Yes")
        self.chk_recursive.SetValue(wx.CHK_CHECKED)
        pnl = wx.Panel (self, -1)
        self.rb_add     = wx.RadioButton (pnl, -1, "Add fonts to group, overwrite duplicates")
        self.rb_add2    = wx.RadioButton (pnl, -1, "Add fonts to group, create new if duplicate")
        self.rb_replace = wx.RadioButton (pnl, -1, "Replace all fonts in group")
        self.rb_replace.SetValue (True)
        rb_sizer = wx.BoxSizer (wx.VERTICAL)
        rb_sizer.Add(self.rb_replace, 1, wx.EXPAND | wx.BOTTOM,8)
        rb_sizer.Add(self.rb_add, 1,     wx.EXPAND | wx.BOTTOM,8)
        rb_sizer.Add(self.rb_add2, 1,    wx.EXPAND | wx.BOTTOM,8)
        pnl.SetSizer(rb_sizer)

        sizer.Add(self.list_directory, pos=(0,1), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(self.chk_recursive,  pos=(1,1), flag=wx.ALL, border=8)
        sizer.Add(pnl,                 pos=(2,1), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add((0,0), pos=(3,0), flag=wx.ALL | wx.EXPAND, span=(1,2), border=8)

        horSizer = wx.BoxSizer()
        horSizer.Add (wx.Button (self, wx.ID_OK,"Ok"),1, wx.EXPAND | wx.ALL, 8)
        horSizer.Add (wx.Button (self, wx.ID_CANCEL,"Cancel"),1, wx.EXPAND | wx.ALL, 8)

        hs1 = wx.BoxSizer(wx.HORIZONTAL)
        hs1.Add( (0,0),1, wx.EXPAND)
        hs1.Add (horSizer, wx.CENTER)
        hs1.Add( (0,0),1, wx.EXPAND)

        sizer.Add(hs1, pos=(4,0), flag=wx.ALL | wx.EXPAND, span=(1,2), border=5)

        bs = wx.BoxSizer(wx.HORIZONTAL)
        bs.Add (sizer, 1, wx.ALL | wx.EXPAND, border=16)
        self.SetSizer(bs)
        bs.Fit(self)

    def GetValue(self):
        try:
            res = {}
            res["path"]          = self.list_directory.GetPath()
            res["recursive"]     = self.chk_recursive.GetValue()
            res["add_overwrite"] = self.rb_add.GetValue()
            res["add_duplicate"] = self.rb_add2.GetValue()
            res["replace"]       = self.rb_replace.GetValue()
            return res
        except:
            return None

class PreferencesDialog(wx.Dialog   ):
    def __init__ (self, *a, **k):
        wx.Panel.__init__(self,*a,**k)
        self.create_controls()

    def create_controls(self):
        sizer = wx.GridBagSizer(8,8)
        sizer.Add(wx.StaticText(self, -1, label="Font list sample text", style=wx.ALIGN_RIGHT)      ,pos=(0,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Font list sample font size", style=wx.ALIGN_RIGHT) ,pos=(1,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Specimen text", style=wx.ALIGN_RIGHT)              ,pos=(2,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Specimen font size", style=wx.ALIGN_RIGHT)         ,pos=(3,0), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(wx.StaticText(self, -1, label="Latex fontbook template", style=wx.ALIGN_RIGHT)    ,pos=(4,0), flag=wx.ALL | wx.EXPAND, border=8)
        
        self.singleLineText = wx.TextCtrl(self, -1, size=(550,70), style=wx.TE_MULTILINE | wx.TE_LEFT, value=Preferences.SINGLE_LINE_TEXT)
        self.specimenText = wx.TextCtrl(self, -1, size=(550,200), style=wx.TE_MULTILINE | wx.TE_LEFT, value=Preferences.SPECIMEN_TEXT)
        self.singleLineFontSize = wx.SpinCtrl(self, -1, size=(120,-1),min=18,max=96,initial=Preferences.SINGLE_LINE_FONT_SIZE)
        self.specimenFontSize = wx.SpinCtrl(self, -1, size=(120,-1),min=18,max=96,initial=Preferences.SPECIMEN_FONT_SIZE)

        files = [f.replace("\\","/").replace("./latex-templates/","")  for f in glob.glob("./latex-templates/*.tex", recursive=False)]
        
        self.fontbookTemplate = wx.ComboBox(self, -1, choices=files, value=Preferences.LATEX_FONTBOOK_FILE, size=(200,-1), style=wx.CB_DROPDOWN | wx.CB_READONLY )

        sizer.Add(self.singleLineText, pos=(0,1), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(self.singleLineFontSize, pos=(1,1), flag=wx.ALL, border=8)
        sizer.Add(self.specimenText, pos=(2,1), flag=wx.ALL | wx.EXPAND, border=8)
        sizer.Add(self.specimenFontSize, pos=(3,1), flag=wx.ALL, border=8)
        sizer.Add(self.fontbookTemplate, pos=(4,1), flag=wx.ALL, border=8)
        sizer.Add((0,0), pos=(5,0), flag=wx.ALL | wx.EXPAND, span=(1,2), border=8)
        sizer.AddGrowableCol(1)

        horSizer = wx.BoxSizer()
        horSizer.Add (wx.Button (self, wx.ID_OK,"Ok"),1, wx.EXPAND | wx.ALL, 8)
        horSizer.Add (wx.Button (self, wx.ID_CANCEL,"Cancel"),1, wx.EXPAND | wx.ALL, 8)

        hs1 = wx.BoxSizer(wx.HORIZONTAL)
        hs1.Add( (0,0),1, wx.EXPAND)
        hs1.Add (horSizer, wx.CENTER)
        hs1.Add( (0,0),1, wx.EXPAND)

        sizer.Add(hs1, pos=(6,0), flag=wx.ALL | wx.EXPAND, span=(1,2), border=5)

        bs = wx.BoxSizer(wx.HORIZONTAL)
        bs.Add (sizer, 1, wx.ALL | wx.EXPAND, border=16)
        self.SetSizer(bs)
        bs.Fit(self)

    def GetValue(self):
        Preferences.SINGLE_LINE_FONT_SIZE = self.singleLineFontSize.GetValue()
        Preferences.SINGLE_LINE_TEXT = self.singleLineText.GetValue()
        Preferences.SPECIMEN_FONT_SIZE = self.specimenFontSize.GetValue()
        Preferences.SPECIMEN_TEXT = self.specimenText.GetValue()
        Preferences.LATEX_FONTBOOK_FILE = self.fontbookTemplate.GetValue()
        print (Preferences.SINGLE_LINE_TEXT, self.singleLineText.GetValue())
        return True

OnChangeDirEvent,      EVT_FONTDIR_CHANGEDIR      = wx.lib.newevent.NewCommandEvent()
OnSelectedFileEvent,   EVT_FONTDIR_SELECTEDFILE   = wx.lib.newevent.NewCommandEvent()
OnTreeSelChangedEvent, EVT_FONTDIR_TREESELCHANGED = wx.lib.newevent.NewCommandEvent()

class FontDirectoryPanel(wx.Panel):
    def __init__ (self, *a, **k):
        wx.Panel.__init__(self,*a,**k)
        self.font_files = []
        self.create_controls()
        self.bind_events()

    def _makeListBoxFontPanel(self,pnlParent):
        pnl = wx.Panel(pnlParent,-1)
        self.list_box_font = wx.ListBox(pnl,-1,style=wx.LB_SORT | wx.LB_SINGLE)
        s =wx.BoxSizer()
        s.Add(self.list_box_font, 1, wx.EXPAND)
        pnl.SetSizer(s)
        return pnl

    def _makeTreeViewFontPanel(self,pnlParent):
        pnl = wx.Panel(pnlParent,-1)
        self.tv_font_panel = gizmos.TreeListCtrl(pnl, -1, style=0, agwStyle=
                                        gizmos.TR_DEFAULT_STYLE
                                        | gizmos.TR_HAS_BUTTONS
                                        | gizmos.TR_TWIST_BUTTONS
                                        | gizmos.TR_FULL_ROW_HIGHLIGHT
                                   )
        self.tv_font_panel.AddColumn("Family/Style")
        self.tv_font_panel.AddColumn("Font File")
        self.tv_font_panel.AddColumn("Accents ?")
        self.tv_font_panel.AddColumn("Euro ?")
        self.tv_font_panel.SetMainColumn(0) # the one with the tree in it...
        self.tv_font_panel.SetColumnWidth(0, 250)

        s =wx.BoxSizer()
        s.Add(self.tv_font_panel, 1, wx.EXPAND)
        pnl.SetSizer(s)
        return pnl

    def create_controls(self):
        pnl = self
        splitter = wx.SplitterWindow(pnl)
        pDirPanel = wx.Panel(splitter)
        pFontPanel = wx.Panel(splitter)
        self.list_directory = wx.GenericDirCtrl(pDirPanel,-1, style=wx.DIRCTRL_DIR_ONLY | wx.DIRCTRL_3D_INTERNAL)
        s = wx.BoxSizer(wx.VERTICAL)
        s.Add(wx.StaticText(pDirPanel,-1, label="Directories and file systems"),0,wx.EXPAND | wx.BOTTOM, border=5)
        s.Add(self.list_directory, 1, wx.EXPAND)
        pDirPanel.SetSizer(s)

        nb = wx.Notebook(pFontPanel,-1)
        pnlFontPage = self._makeListBoxFontPanel(nb)
        pnlFontTV = self._makeTreeViewFontPanel(nb)

        nb.InsertPage(0, pnlFontPage, "Font Files")
        nb.InsertPage(1, pnlFontTV,   "Families and Styles")

        s = wx.BoxSizer(wx.VERTICAL)
        s.Add(wx.StaticText(pFontPanel,-1, label="Font files and families"),0,wx.EXPAND | wx.BOTTOM | wx.TOP, border=5)
        s.Add(nb, 1, wx.EXPAND)
        pFontPanel.SetSizer(s)
        
        splitter.SplitHorizontally(pDirPanel,pFontPanel)
        splitter.SetMinimumPaneSize(300)
        
        s1 = wx.BoxSizer()
        s1.Add(splitter,1,wx.EXPAND)
        pnl.SetSizer(s1)
        self.list_box_font.SetSize(300,100)

    def bind_events(self):
        self.Bind(wx.EVT_LISTBOX,        self.on_font_list_double_click, self.list_box_font)
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.on_font_list_double_click, self.list_box_font)
        self.Bind(wx.EVT_DIRCTRL_SELECTIONCHANGED, self.on_change_dir, self.list_directory)
        self.tv_font_panel.GetMainWindow().Bind(wx.EVT_TREE_SEL_CHANGED, self.on_tree_sel_changed)

    def on_font_list_double_click (self, evt):
        ev = OnSelectedFileEvent(self.GetId(), filename=self.list_box_font.GetStringSelection())
        self.GetEventHandler().ProcessEvent(ev)

    def on_tree_sel_changed (self, evt):
        item=evt.GetItem()
        ev = OnTreeSelChangedEvent (self.GetId()
            , item=item
            , filename=self.tv_font_panel.GetItemText(item,1))
        self.GetEventHandler().ProcessEvent(ev)
        # try:
        #     if self.OnTreeSelChanged != None:
        #         item = evt.GetItem()
        #         data = self.tv_font_panel.GetItemText(item,1)
        #         if data != None and data != "":
        #             self.OnTreeSelChanged(data)
        # except Exception as e:
        #     print (e)

    def on_change_dir (self, evt):
        ev = OnChangeDirEvent(self.GetId(), path=self.list_directory.GetPath())
        self.GetEventHandler().ProcessEvent(ev)
        # wx.PostEvent(self.Parent,OnChangeDirEvent(
        #         data=self.list_directory.GetPath()
        #     ))

    def GetStringSelection(self):
        return self.list_box_font.GetStringSelection()

    def Set(self, list_data):
        self.list_box_font.Set(list_data)

    def GetPath(self):
        return self.list_directory.GetPath()

    def ReadFontDir(self):
        try:
            files = []
            for file in os.listdir(self.GetPath()):
                if file.lower().endswith("ttf") or file.lower().endswith("otf") or file.lower().endswith("pfb"):
                    files.append(file)
            self.font_path = self.GetPath()
            self.font_files = files
            self.Set(files)
        except Exception as a:
            print (a)    
        self.ExtractInfosFromFontsInDir()

    def ExtractInfosFromFontsInDir(self):
        self.families = {}
        prog_dlg = None
        cnt = 0
        wx.YieldIfNeeded()
        if len (self.font_files) > 20:
            prog_dlg = wx.ProgressDialog("Examining font files:", "Examining", len (self.font_files)*2,
                style=wx.PD_CAN_ABORT | wx.PD_ELAPSED_TIME | wx.PD_REMAINING_TIME)
        try:
            for ffile in self.font_files:
                try:
                    ffName = self.font_path+"/"+ffile
                    fnt = ImageFont.truetype(ffName, 16)
                    family = fnt.font.family
                    style = fnt.font.style
                    if family in self.families:
                        self.families[family][style]=ffile
                    else:
                        self.families[family]={style:ffile}
                    fnt = None
                    if prog_dlg != None:
                        cnt = cnt +1
                        prog_dlg.Update(cnt)
                except Exception as aFont:
                    print ("Error reading ",ffName)
            self.tv_font_panel.DeleteAllItems()
            root = self.tv_font_panel.AddRoot("Fonts")
            for _family in sorted(self.families.keys()):
                fItem = self.tv_font_panel.AppendItem(root,_family)
                weights = self.families[_family]
                for sty in weights.keys():
                    try:
                        id_sty = self.tv_font_panel.AppendItem(fItem, sty)
                        self.tv_font_panel.SetItemText(id_sty,weights[sty],1)
                        print (self.font_path+ "/" + weights[sty])
                        fData = FontData(self.font_path+ "/" + weights[sty] ,style=sty, family=_family)
                        self.tv_font_panel.SetItemText(id_sty,str(fData.has_accents),2)
                        self.tv_font_panel.SetItemText(id_sty,str(fData.has_euro),3)
                    except Exception as eAccens:
                        print ("Eaccnes...",eAccens)
                        print (eAccens.__cause__)
                    if prog_dlg != None:
                        cnt = cnt +1
                        prog_dlg.Update(cnt)
                if "regular" in weights.keys():
                    self.tv_font_panel.SetItemText(fItem,weights["regular"],1)
                elif "Regular" in weights.keys():
                    self.tv_font_panel.SetItemText(fItem,weights["Regular"],1)
                elif "Book" in weights.keys():
                    self.tv_font_panel.SetItemText(fItem,weights["Book"],1)
                elif "Roman" in weights.keys():
                    self.tv_font_panel.SetItemText(fItem,weights["Roman"],1)
                else:                                    
                    self.tv_font_panel.SetItemText(fItem,weights[next(iter(weights))],1)
        except Exception as aMain:
            print ("EXEXEX!", aMain)
        finally:
            if prog_dlg != None:
                prog_dlg.Close()
                prog_dlg.Destroy()
                prog_dlg = None
        if prog_dlg != None:
            prog_dlg.Close()
            prog_dlg.Destroy()
            prog_dlg = None
        


OnFontSizeChangedEvent,    EVT_FONTINFO_CHANGESIZE     = wx.lib.newevent.NewCommandEvent()
OnTextChangedEvent,        EVT_FONTINFO_SELECTEDFILE   = wx.lib.newevent.NewCommandEvent()


"""
Shows basic information about the selected font: family, style. It also incorporates
a slider allowing the user to change font size and a text box containing the dummy text
"""
class FontInfoPanel(wx.Panel):
    def __init__ (self, *a, **k):
        wx.Panel.__init__(self,*a,**k)
        self.create_controls()
        self.bind_events()

    def create_controls(self):
        p = self
        sizer = wx.BoxSizer (wx.HORIZONTAL)
        self.font_name = wx.StaticText(p,-1, size=(200,20))
        self.font_style = wx.StaticText(p,-1, size=(130,20))
        self.text_info = wx.TextCtrl(p,-1, style=wx.TE_MULTILINE,size=(100,100))
        self.text_info.SetValue(Preferences.SPECIMEN_TEXT)

        sizer.Add( wx.StaticText(p,-1,"Font name: "),0,wx.ALL, 5)
        sizer.Add( self.font_name,0,wx.ALL  , 5,10)
        sizer.Add( wx.StaticText(p,-1,"    Font style: "),0,wx.ALL, 5)
        sizer.Add( self.font_style,0,wx.ALL , 5,10)
        sizer.Add( wx.StaticText(p,-1,"    Font Size: "),0,wx.ALL, 5)

        self.combo_font_size = wx.ComboBox(p,-1,size=(100,-1),value="30",choices=["16","20","24","30","36","44","52","64","80","96"],style=wx.CB_DROPDOWN)
        sizer.Add( self.combo_font_size,0,wx.ALL | wx.EXPAND , 5,50)
        sizer2 = wx.BoxSizer (wx.VERTICAL)
        sizer2.Add (sizer,          0,  wx.EXPAND        )
        sizer2.Add (self.text_info, 1,  wx.ALL | wx.EXPAND,5)
        p.SetSizer(sizer2)

    def GetTextValue(self):
        return self.text_info.GetValue()

    def GetComboValue(self):
        return self.combo_font_size.GetValue()

    def bind_events(self):
        self.Bind(wx.EVT_KILL_FOCUS,   self.on_text_changed,   self.text_info )
        self.Bind(wx.EVT_COMBOBOX,     self.on_combo_changed, self.combo_font_size )
        print ("Font info panel events bound")
        pass
    
    def on_combo_changed (self, evt):
        size = 24
        try:
            size = int(self.GetComboValue())
        except Exception as a:
            print (a)
        ev = OnFontSizeChangedEvent(self.GetId(), value=size )
        self.GetEventHandler().ProcessEvent(ev)

    def on_text_changed (self, evt):
        ev = OnTextChangedEvent(self.GetId(), value=self.GetTextValue() )
        self.GetEventHandler().ProcessEvent(ev)



class PreferencesPanel(wx.Panel):
    def __init__ (self, *a, **k):
        wx.Panel.__init__(self,*a,**k)