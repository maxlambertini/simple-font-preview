import wx
import wx.lib.scrolledpanel as scrolled
import wx.html2
from PIL import Image, ImageDraw, ImageFont
import glob, os, sys
from controls.Structs import Preferences

class ScrolledBitmapContainer(wx.ScrolledWindow):
    def __init__(self, *a,**k):
        wx.ScrolledWindow.__init__(self,*a,**k)
        self.SetBackgroundColour("white")
        self.bitmap_viewer = wx.StaticBitmap(self,-1)
        self.tmp_path = wx.StandardPaths.Get().GetTempDir()
        self._path = "."
        self._file = ""
        self._size = 32
        self.old_path = ""
        self._sample_text = Preferences.SPECIMEN_TEXT

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self,p):
        self._path = p

    @property
    def file(self):
        return self._file

    @file.setter 
    def file(self,p):
        self._file = p

    @property
    def size(self):
        return self._size

    @size.setter 
    def size(self,p):
        self._size = p

    @property
    def sample_text(self):
        return self._sample_text

    @sample_text.setter
    def sample_text(self,p):
        self._sample_text = p


    def specimen_sheet(self, text=None):
        filename = self._file    
        full_path = self.path+"/"+filename
        full_img =  self.tmp_path +"/__"+filename+".png"
        p_text = text
        if p_text == None:
            p_text = self._sample_text
        p_lines = p_text.split("\n")
        longest_line = ""
        for p_l in p_lines:
            if len(p_l) > len(longest_line):
                longest_line = p_l
        
        try:
            infos = []
            tot_width = 0
            tot_height = 0
            font = ImageFont.truetype(full_path, self.size)
            wd,hd = font.getsize (longest_line)
            lines = [font.font.family+" "+font.font.style]
            for p_l in p_lines:
                lines.append(p_l)

            for line in lines:
                w1, h1 = font.getsize(line)
                f1 = ImageFont.truetype (full_path, int (self.size * wd / w1))
                width, height = f1.getsize(line)
                w2,h2 = f1.getsize("Pj")
                infos.append((line,f1,width,h2+8))

            tot_height = 0
            tot_width = 0
            for i in infos:
                tot_height = tot_height+i[3]+10
                w = i[2]
                if (10+w) > tot_width:
                    tot_width = 10+w

            canvas = Image.new('RGB', (tot_width + 10, tot_height  + 10), "white")
            draw = ImageDraw.Draw(canvas)

            dw = 5
            dh = 5
            for i in infos:
                t_line = i[0]
                t_font = i[1]
                h_font = i[3]
                draw.text((dw, dh), t_line, 'black', t_font)
                dh = dh + h_font

            canvas.save(full_img, "PNG")
            fnt = font.font
            #print ("Processed spec....",filename,fnt.family, fnt.style, fnt.ascent, fnt.descent, fnt.height, fnt.x_ppem, fnt.y_ppem )
            
            self._update_bitmap_control(tot_width, tot_height, full_img)
            return (font.font.family, font.font.style)                                    
        except Exception as ex:
            print (ex)
            return None


    def get_font_info(self):
        filename = self._file    
        full_path = self.path+"/"+filename
        full_img =  self.tmp_path +"/__"+filename+".png"
        font = ImageFont.truetype(full_path, 1)
        data = (font.font.family, font.font.style)
        font = None
        return data

    def create_bitmap(self):
        filename = self._file    
        full_path = self.path+"/"+filename
        full_img =  self.tmp_path +"/__"+filename+".png"
        if (self.old_path != ""):
            try:
                print ("Removing ",self.old_path)
                os.remove(self.old_path)
            except Exception as exc:
                print (exc)
        try:
            print (full_path)
            sample_text = self._sample_text
            font = ImageFont.truetype(full_path, self.size)
            text_width, text_height = (0,0)
            lines = sample_text.split('\n')
            for line in lines:
                t_text_width, t_text_height = font.getsize(line)
                print (t_text_width, t_text_height)
                if t_text_width > text_width:
                    text_width = t_text_width
                text_height = text_height + font.font.height + 10
            canvas = Image.new('RGB', (text_width + 10, text_height  + 10), "white")
            draw = ImageDraw.Draw(canvas)
            draw.text((5, 5), sample_text, 'black', font)
            canvas.save(full_img, "PNG")
            fnt = font.font
            #print ("Processed....",filename,fnt.family, fnt.style, fnt.ascent, fnt.descent, fnt.height, fnt.x_ppem, fnt.y_ppem )

            self._update_bitmap_control(text_width, text_height, full_img)
            return (font.font.family, font.font.style)
        except Exception as ex:
            print (ex)
            return None



    def _update_bitmap_control(self, text_width, text_height, full_img):
        self.Hide()
        unit = 20 
        width,height = self.bitmap_viewer.GetSize() 
        self.SetVirtualSize((width+unit, height+unit)) 
        self.SetScrollRate(unit, unit) 
        self.bitmap_viewer.Hide()
        self.bitmap_viewer.SetBackgroundColour(wx.Colour(255,255,255))
        bmp = wx.BitmapFromImage( full_img)
        if wx.TheClipboard.Open():
            wx.TheClipboard.SetData(wx.BitmapDataObject(bmp))
            wx.TheClipboard.Close()
        self.bitmap_viewer.SetBitmap ( bmp)
        self.bitmap_viewer.SetSize(text_width + 10, text_height  + 10)
        self.SetVirtualSize((text_width + 10+unit, text_height+10+unit)) 
        self.bitmap_viewer.Update()
        self.bitmap_viewer.Refresh()
        self.bitmap_viewer.Show()
        self.Show()
        self.old_path = full_img


