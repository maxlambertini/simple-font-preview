from fontTools.t1Lib import T1Font
from fontTools.ttLib import TTFont

def extractData(d, idx):
    try:
        w = d[idx]
        if b'\x00' in w:
            return w.string.decode('utf-16-be')
        else:
            return w.string.decode('latin-1')
    except:
        return ''

class FontData(object):
    def __init__(self,path,style="",family=""):
        self.data  = None
        self.has_accents = False
        self.has_euro = False
        self.family = family
        self.style = style
        self.complete_name = ""
        self.copyright = ""
        self.short_name = ""
        self.author = ""
        self.tm = ""
        try:
            if path.lower().endswith(".ttf") or path.lower().endswith(".otf"):
                data = TTFont(path)
                self.copyright = extractData(data['name'].names,0)
                self.family = extractData(data['name'].names,1)
                self.style = extractData(data['name'].names,2)
                self.complete_name = extractData(data['name'].names,4)
                self.short_name = extractData(data['name'].names,6)
                self.tm = extractData(data['name'].names,7)
                self.author = extractData(data['name'].names,8)
                self.has_accents = 'egrave' in data.getGlyphSet() 
                self.has_euro = 'Euro' in data.getGlyphSet()
                print ("DF:",self.family)
                data = None 
                return
            if path.lower().endswith(".pfb"):
                data = T1Font(path)
                self.has_accents = 'egrave' in data.getGlyphSet() 
                self.has_euro = 'Euro' in data.getGlyphSet()
                data = None 
                return 
        except Exception as e:
            print ("Error opening font ",path, e)
            pass

if __name__=="main":
    pass