import wx
import wx.lib.newevent
from PIL import Image, ImageDraw, ImageFont
import glob, os, sys
import queue
from controls.Fonts import FontData
from controls.Structs import Preferences, LATEX_TEMPLATE, get_latex_template
from controls.Structs import int_to_roman
import json
from cachetools import cached, TTLCache 
cache = TTLCache(maxsize=30, ttl=300)

OnFontPathChangedEvent,     EVT_VFONTLIST_FONTPATHCHANGED    = wx.lib.newevent.NewCommandEvent()
OnPreviewSizeChangedEvent,  EVT_VFONTLIST_PREVIEWSIZECHANGED = wx.lib.newevent.NewCommandEvent()
OnVFontListEvent,           EVT_VFONTLIST                    = wx.lib.newevent.NewCommandEvent()

"""
This class represents a physical font file. It encapsulates, at its most basic:

- its path
- its filename
- a bitmap containing its test

"""
class FontItem(object):
    """
    Creates a FontItem object

    Parameters:
        font_path (string): the directory containing the font file
        font_file (string): the font filename. It must be either an Opentype font, 
        a Truetype font, or a T1 PFB font.  
    """
    def __init__ (self, font_path, font_file):
        self.font_file = font_file
        self.font_path = font_path
        self.font_bitmap = None
        self.selected = True
        self.font_family = ""
        self.font_style = ""

    def from_dictionary(dict):
        try:
            font_file = dict["font_file"]
            font_path = dict["font_path"]
            font_family = dict["font_family"]
            font_style  = dict["font_style"]
            selected  = dict["selected"]
            fi = FontItem(font_path, font_file)
            fi.font_family = font_family
            fi.font_style = font_style
            fi.selected = selected
            return fi
        except Exception as e:
            return None

    @property
    def full_name(self):
        return "%s %s" % (self.font_family, self.font_style)

    def read_infos(self,size=24):
        self.font_bitmap = None
        self.full_path = "%s/%s" % (self.font_path,self.font_file)
        self.font = ImageFont.truetype(self.full_path,size)
        self.font_family = self.font.font.family
        self.font_style = self.font.font.style
        del self.font
        self.font = None

    def to_dictionary (self):
        return {
            "font_file"   : self.font_file,
            "font_path"   : self.font_path,
            "font_family" : self.font_family,
            "font_style"  : self.font_style,
            "selected"    : self.selected
        }

    

    @cached(cache=cache)
    def generate_bitmap(self,size=24,full_path=None):
        try:
            font = ImageFont.truetype(full_path,size)
            text_width, text_height = (0,0)
            lines = Preferences.SINGLE_LINE_TEXT.split('\n')
            for line in lines:
                t_text_width, t_text_height = font.getsize(line)
                #print (t_text_width, t_text_height)
                if t_text_width > text_width:
                    text_width = t_text_width
                text_height = text_height + font.font.height + 10
            canvas = Image.new('RGBA', (text_width + 10, text_height  + 10),(0,0,0,0))
            draw = ImageDraw.Draw(canvas,"RGBA")
            draw.text((5, 5), Preferences.SINGLE_LINE_TEXT, 'black', font)
            bitmap_height= text_height + 10
            bitmap_width= text_width + 10
            del font
            font = None
            return (wx.Bitmap.FromBufferRGBA(bitmap_width, bitmap_height, canvas.tobytes()), bitmap_width, bitmap_height)

        except Exception as exc:
            print ("Error in generate_bitmap:", full_path, self.full_name, exc)


class FontItems(object):
    def __init__ (self, path=".", name=None):
        self._path = path
        if name == None:
            self.name = self._path
        else:
            self.name = name
        self.fonts = {}

    def to_dictionary(self):
        res = {
            "path": self._path,
            "name": self.name,
        }
        if self.fonts != None:
            dictfonts = {}
            for k in sorted(self.fonts.keys()):
                dictfonts[k] = self.fonts[k].to_dictionary()
            res["fonts"] = dictfonts 
        return res

    def to_json(self):
        return json.dumps(self.to_dictionary())

    def from_directory(path, name="New Group", recursive=False):
        fi = FontItems()
        if not recursive:
            fi.name = name
            fi.set_path(path, False)
        else:
            fi.name = name
            fi.set_recursive_path(path)
        return fi

    def from_dictionary(dict, load_info=True, callback=None):
        fi = FontItems()
        fi._path = dict["path"]
        fi.name = dict["name"]
        if "fonts" in dict:
            dictfonts = dict["fonts"]
            for k in dictfonts.keys():
                fi.fonts[k] = FontItem.from_dictionary(dictfonts[k])
                if load_info:
                    fi.fonts[k].read_infos()
                if callback != None:
                    callback(k,fi)
        return fi
    
    def from_json(dict_data):
        return FontItems.from_dictionary(json.loads(dict_data))


    @property
    def path(self):
        return self._path

    def set_recursive_path(self,path, load_preview = True):
        self._path = path
        if (self.fonts != None):
            for f in self.fonts:
                self.font_bitmap.Destroy()
        del self.fonts
        self.fonts = {}
        for root, dirs, files in os.walk(path):
            base_path = root.replace("\\","/")
            idx = 1
            for file in files:
                if file.lower().endswith("ttf") or file.lower().endswith("otf") or file.lower().endswith("pfb"):
                    try:                            
                        fnt = FontItem (base_path,file)
                        if load_preview:
                            fnt.read_infos(36)
                        self.fonts[fnt.full_name] = fnt
                        idx = idx+1
                    except Exception as eFont:
                        print ("Error reading font: ",base_path,file,eFont)

    def set_path(self, p, load_preview=True):
        self._path = p
        if (self.fonts != None):
            for f in self.fonts:
                self.font_bitmap.Destroy()
        del self.fonts
        self.fonts = {}
        idx = 1
        for file in os.listdir(p):
            if file.lower().endswith("ttf") or file.lower().endswith("otf") or file.lower().endswith("pfb"):
                try:
                    fi = FontItem (p,file)
                    if load_preview:
                        fi.read_infos(36)
                    self.fonts[fi.full_name] = fi
                    idx = idx+1
                except Exception as eFont:
                    print ("Error reading font: ",p,file,eFont)

    @path.setter
    def path(self, p):
        self.set_path(p,True)
 
    def AddFontFromFile(self, path, file):
        try:
            fi = FontItem(path, file)
            fi.read_infos(36);
            self.fonts=[fi.full_name] = fi
        except Exception as e:
            print ("Error in adding font file to group: ", self.group_name, e)

    def AddFont(self, fi):
        try:
            self.fonts=[fi.full_name] = fi
        except Exception as e:
            print ("Error in adding font to group: ", self.group_name, e)

    def RemoveFont(self, full_name):
        try:
            self._fonts.pop(full_name)
        except Exception as e:
            print ("Error in removing font from group: ", self.group_name, e)


    def returnFontDefs(self):
        fd = []
        DEF_TPL = "\\newfontfamily{\\font%s}[Path = %s/, Ligatures=TeX, UprightFont=%s]{}"
        cnt = 1
        for k in sorted(self.fonts.keys()):
            fi = self.fonts[k]
            if fi != None and fi.selected:
                fd.append(DEF_TPL % (int_to_roman(cnt), fi.font_path.replace("\\","/"),  fi.font_file))
                cnt = cnt+1
        return "\n".join(fd)

    def returnFontInvoke(self):
        fd = []
        DEF_TPL = "\\demoText{\\font%s}{%s} "
        cnt = 1
        for k in sorted(self.fonts.keys()):
            fi = self.fonts[k]
            if fi != None and fi.selected:
                fd.append(DEF_TPL % (int_to_roman(cnt) , k))
                cnt = cnt+1
        return "\n".join(fd)

    def returnLatexFile(self):
        tpl = get_latex_template( Preferences.LATEX_FONTBOOK_FILE ).replace("{","{{").replace("}","}}").replace("<--","{").replace("-->","}")
        print (tpl)
        tpl2 = tpl.format(fontdefs=self.returnFontDefs(), templatepages=self.returnFontInvoke() )
        return tpl2

class VFontListBox(wx.VListBox):
    def __init__ (self, *a, **k):
        wx.VListBox.__init__(self,*a,**k)
        self._font_items = None
        self._sorted_keys = None
        self._preview_size = 36
        self._lifo_queue = queue.Queue(40)
        self.Bind(wx.EVT_LISTBOX, self.on_vfont_list_event)

    """
    Main handler for EVT_LISTBOX, that propagates custom event. 
    """
    def on_vfont_list_event (self, evt):
        n,cookie = self.GetFirstSelected()
        if  self._font_items != None and self._font_items.fonts != None:
            if n >= 0 and n < len(self._font_items.fonts):   
                fi = self._font_items.fonts[self._sorted_keys[n]]
                ev =OnVFontListEvent(self.GetId(), 
                    list_event=evt, 
                    family=fi.font_family,
                    style=fi.font_style,
                    path=fi.full_path,
                    file=fi.font_file)
                self.GetEventHandler().ProcessEvent(ev)

    @property
    def font_items(self):
        return self._font_items

    @font_items.setter
    def font_items(self, p):
        self._font_items = p
        self._sorted_keys = list(sorted(self._font_items.fonts.keys()))
        self._bmp_cache = {}

    def OnMeasureItem(self, n):
        #print (self.font_items)
        if self._font_items == "None":
            return 24
        else:
            fi = self._font_items.fonts[self._sorted_keys[n]]
            (bmp,w,h) = fi.generate_bitmap(self._preview_size, fi.full_path)
            return h
        return 24

    def OnDrawItem(self,dc,rect,n):
        #
        #first, draw background
        #   
        c1 = wx.Colour(248,248,248)
        c2 = wx.Colour(224,224,224)
        cx = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
        cx = cx.ChangeLightness(150)
        gc = wx.GraphicsContext.Create(dc)
        if self.IsSelected(n):
            b = gc.CreateLinearGradientBrush (rect.Left, rect.Top, rect.Left, rect.Bottom,c1,cx)
            gc.SetBrush(b)
            gc.DrawRectangle(rect.Left, rect.Top+1, rect.Right-rect.Left, (rect.Bottom-rect.Top)-1)
            pass
        else:
            b = gc.CreateLinearGradientBrush (rect.Left, rect.Top, rect.Left, rect.Bottom,c1,c2)
            gc.SetBrush(b)
            gc.DrawRectangle(rect.Left, rect.Top+1, rect.Right-rect.Left, (rect.Bottom-rect.Top)-1)
        #
        # Then, everything else
        #
        if self._font_items != None:            
            if self._font_items != None and n < len(self._font_items.fonts):                
                if dc.CanDrawBitmap():
                    fi = self._font_items.fonts[self._sorted_keys[n]]                                            
                    (bmp,w,h) = fi.generate_bitmap(self._preview_size, fi.full_path)
                    if (bmp != None):
                        dc.DrawBitmap(bmp,rect.Left,rect.Top+14)
                    dc.DrawText(fi.full_name, rect.Left+25, rect.Top)
                    self._draw_selection_box(gc, rect)
                    if fi.selected:
                        self._draw_selection_tick(gc, rect)

    def _draw_selection_tick(self, gc, rect):
        p = wx.GREEN_PEN
        p.SetWidth(4)
        p.SetCap(wx.CAP_ROUND)
        gc.SetPen(p)
        path = gc.CreatePath()
        x = rect.Left
        y = rect.Top+5
        path.MoveToPoint (x+4, y+8)
        path.AddLineToPoint (x+5,y+15)
        path.AddLineToPoint (x+16,y+5)
        gc.StrokePath(path)


    def _draw_selection_box (self, gc, rect):
        p = wx.BLACK_PEN
        p.SetWidth(2)
        x = rect.Left
        y = rect.Top
        gc.SetPen(p)
        path = gc.CreatePath()
        path.MoveToPoint(x+4,y+4)
        path.AddLineToPoint(x+18,y+4)
        path.AddLineToPoint(x+18,y+18)
        path.AddLineToPoint(x+4,y+18)
        path.AddLineToPoint(x+4,y+4)
        gc.StrokePath(path)


    def ToggleFontSelection(self):
        if self._font_items != None:
            n, cookie = self.GetFirstSelected()            
            while n != wx.NOT_FOUND:
                fi = self.font_items.fonts[self._sorted_keys[n]]
                if (fi != None):
                    fi.selected = not fi.selected
                    self.Refresh(True)
                n,cookie = self.GetNextSelected(cookie)    

    def InvertSelection(self):
        if self._font_items != None:  
            for k in self._sorted_keys:
                fi = self.font_items.fonts[k]
                fi.selected = not fi.selected
            self.Refresh(True)

    def SetSelectionForAllFont(self, selectionFlag = True):
        if self._font_items != None:  
            for k in self._sorted_keys:
                fi = self.font_items.fonts[k]
                fi.selected = selectionFlag
            self.Refresh(True)

    def SelectAllFont(self):
        self.SetSelectionForAllFont(True)

    def UnSelectAllFont(self):
        self.SetSelectionForAllFont(False)

    def SetPreviewSize(self, new_size):
        try:
            self._preview_size = new_size
            cache.clear()
            print ("Cache flushed")
            self._bmp_cache.clear()
            #self.SetSelection(0)
            self.RefreshAll()
            path = ""
            if self.font_items != None:
                path = self.font_items.path
            ev =OnPreviewSizeChangedEvent(self.GetId(), path=path, size=self._preview_size)
            self.GetEventHandler().ProcessEvent(ev)

        except Exception as Ex:
            print ("Error SetFontSize ", path, Ex, type(Ex))

    def SetFontPath(self, path):
        try:
            self.SetItemCount(0)
            self.Update()
            wx.Yield()
            fis = FontItems()
            fis.path=path   
            print ("Path set")     
            self.font_items = fis
            print ("FI set")     
            self._bmp_cache.clear()
            self.SetItemCount(len(fis.fonts))
            ev = OnFontPathChangedEvent(self.GetId(), path=path, size=self._preview_size)
            self.GetEventHandler().ProcessEvent(ev)
            
        except Exception as Ex:
            print ("Error SetFontPath ", path, Ex, type(Ex))

    def SelectItemByFont(self, family, style):
        try:
            key = family + " "+style
            print (key)
            idx = self._sorted_keys.index(key)
            print (idx)
            print ("Key index is ", idx)
            self.SetSelection(idx)
        except Exception as e:
            print ("VFontListBox.SelectItemByFont", e)

    def PrintSpecimensToLatex(self, filename):
        try:
            with open(filename, "w", encoding="utf-8") as the_file:
                latex = self._font_items.returnLatexFile()
                the_file.write(latex)
                return True
        except Exception as e:
            print (e)
            return False

if __name__ == "main":
    pass
