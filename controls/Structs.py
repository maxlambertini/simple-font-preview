import random

class Preferences(object):
    SINGLE_LINE_TEXT = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLM\nNOPQRSTUVWXYZ1234567890 àèéìòùç $¢€&!%&/()=?^"
    SINGLE_LINE_FONT_SIZE = 48
    SPECIMEN_FONT_SIZE=32
    SPECIMEN_TEXT = """The Quick Brown: Fox
Jumped, Over The; Lazy Dog.
Tafjlét, Algérie
Eay/€/123
4567890 @#àèìòù
ABCDEFGHIJKLMNOPQRST
UVWXYabcdefghijklmnopq
rstuvwxyz!"£$%&/(){}
Garçon? De l'hétérogénéité, s'il vous plait. 
Sia nel Maharem, che si celebra al principio dell’anno, sia nel Ramadan o
grande o piccolo Beiram, gli affigliati delle diverse sette religiose, per 
guadagnarsi il paradiso, si abbandonano ad eccessi che talvolta fremere.
Gepäckservice oder Sitzplatz-
reservierung, Fahrpläne für Fußballfans"""
    LATEX_FONTBOOK_FILE="alphabet.tex"

def int_to_roman(input):
    """ Convert an integer to a Roman numeral. """

    if not isinstance(input, type(1)):
        raise (TypeError, "expected integer, got %s" % type(input))
    if not 0 < input < 4000:
        raise (ValueError, "Argument must be between 1 and 3999")
    ints = (1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1)
    nums = ('M',  'CM', 'D', 'CD','C', 'XC','L','XL','X','IX','V','IV','I')
    result = []
    for i in range(len(ints)):
        count = int(input / ints[i])
        result.append(nums[i] * count)
        input -= ints[i] * count
    return ''.join(result)

def get_latex_template(texTpl):
    try:
        with open("./latex-templates/"+texTpl,"r",encoding="utf-8") as tex:
            data = tex.read()
            return data
    except Exception as e:
        print (e)
        return None
    
LATEX_TEMPLATE="""\\documentclass[10.5pt,a4paper,sans]{{article}} 
\\usepackage[top=1.5cm,bottom=1.5cm,left=1.5cm,right=1.5cm]{{geometry}}
\\setlength{{\\columnsep}}{{7mm}} 
\\usepackage[italian]{{babel}}
\\usepackage[utf8]{{inputenc}}
\\usepackage[T1]{{fontenc}}
\\usepackage{{lmodern}}
\\usepackage{{fontspec}}
\\usepackage{{parskip}}                                                            % Add spacing between paras instead of indents
\\usepackage{{multicol}}

{fontdefs}


\\newenvironment{{paraTex}}[3]{{#1\\fontsize{{#2}}{{#3}}\\selectfont}}{{}}

\\newcommand{{\\demoText}}[2]{{
{{\\small #2}}
\\vskip -10pt\\rule{{\\textwidth}}{{0.4pt}}\\vskip -10pt

\\begin{{paraTex}}{{#1}}{{28pt}}{{28pt}}
Wim Xavier, questo povero ma bravo disk-jockey che già, non fu solo zarro. A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z à è é ì ò ù ç 1 2 3 4 5 6 7 8 9 s0 ,;:.


\\end{{paraTex}}

\\vskip -2pt\\rule{{\\textwidth}}{{0.4pt}}\\vskip -2pt

\\begin{{paraTex}}{{#1}}{{18pt}}{{22pt}}
Il Gange, questo famoso fiume fluviale affiatato afflato celebrato dagli indiani antichi e moderni, le cui acque son reputate sacre da quei popoli


\\end{{paraTex}}

\\vskip -2pt\\rule{{\\textwidth}}{{0.4pt}}\\vskip -2pt

\\begin{{multicols}}{{2}}
\\begin{{paraTex}}{{#1}}{{14pt}}{{18pt}}
dopo d'aver solcato le nevose montagne dell'Himalaya e le ricche provincie del Sirinagar, di Delhi, di Odhe, di Bahare, di Bengala, a duecentoventi miglia dal mare dividesi in due bracci, formando un delta gigantesco, intricato, meraviglioso e forse unico.


\\end{{paraTex}}
\\end{{multicols}}

\\vskip -2pt\\rule{{\\textwidth}}{{0.4pt}}\\vskip -2pt

\\begin{{multicols}}{{3}}
\\begin{{paraTex}}{{#1}}{{9pt}}{{12pt}}
Il Gange, questo famoso fiume celebrato dagli indiani antichi e moderni, le cui acque son reputate sacre da quei popoli, dopo d'aver solcato le nevose montagne dell'Himalaya e le ricche provincie del Sirinagar, di Delhi, di Odhe, di Bahare, di Bengala, a duecentoventi miglia dal mare dividesi in due bracci, formando un delta gigantesco, intricato, meraviglioso e forse unico.

Nulla di più desolante, di più strano e di più spaventevole che la vista di queste Sunderbunds. Non città, non villaggi, non capanne, non un rifugio qualsiasi; dal sud al nord, dall'est all'ovest, non scorgete che immense piantagioni di bambù spinosi, stretti gli uni contro gli altri, le cui alte cime ondeggiano ai soffi del vento, appestato dalle esalazioni insopportabili di migliaia e migliaia di corpi umani che imputridiscono nelle avvelenate acque dei canali.

È raro se scorgete un banian torreggiare al disopra di quelle gigantesche canne, ancor più raro se v'accade di scorgere un gruppo di manghieri, di giacchieri o di nagassi sorgere fra i pantani, o se vi giunge all'olfatto il soave profumo del gelsomino, dello sciambaga o del mussenda, che spuntano timidamente fra quel caos di vegetali.

\\end{{paraTex}}
\\end{{multicols}}

\\vskip -2pt\\rule{{\\textwidth}}{{0.4pt}}\\vskip -2pt


\\begin{{multicols}}{{4}}
\\begin{{paraTex}}{{#1}}{{7pt}}{{9pt}}
Di giorno, un silenzio gigantesco, funebre, che incute terrore ai più audaci, regna sovrano: di notte invece, è un frastuono orribile di urla, di ruggiti, di sibili e di fischi, che gela il sangue.

Dite al bengalese di porre piede nelle Sunderbunds ed egli si rifiuterà; promettetegli cento, duecento, cinquecento rupie, e mai smuoverete la incrollabile sua decisione.

Dite al molango che vive nelle Sunderbunds, sfidando il cholera e la peste, le febbri ed il veleno di quell'aria appestata, di entrare in quelle jungle ed al pari del bengalese si rifiuterà. Il bengalese ed il molango non hanno torto; inoltrarsi in quelle jungle, è andare incontro alla morte.

\\end{{paraTex}}
\\end{{multicols}}
}}

\\begin{{document}}

{templatepages}

\\end{{document}}"""