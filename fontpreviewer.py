#!/usr/bin/python3
import wx
import wx.lib.scrolledpanel as scrolled
import wx.html2
from PIL import Image, ImageDraw, ImageFont
import glob, os, sys

from controls.Panels import *
from controls.Windows import ScrolledBitmapContainer
from controls.VFontControls import *
from controls.Structs import Preferences

PREVIEW_SIMPLE=0
PREVIEW_SPECIMEN=1
PREVIEW_LIST=2

toolbarIcons = ['folder-open.png',
                'edit-copy.png',
                'printer.png',
                'font-sample.png',
                'font-specimen.png',
                'font-list.png',
                'preferences.png',
                'about.png',
                'application-exit.png',
                'text-x-tex.png']

class FontPreviewerFrame(wx.Frame):
    def __init__ (self, *a, **k):
        wx.Frame.__init__(self,*a,**k)
        ib = wx.IconBundle()
        ib.AddIcon('./icons/sfm-16x16.ico')
        ib.AddIcon('./icons/sfm-32x32.ico')
        ib.AddIcon('./icons/sfm-64x64.ico')
        ib.AddIcon('./icons/sfm-128x128.ico')
        self.SetIcons(ib)
        self.path = "."
        self.tmp_path = wx.StandardPaths.Get().GetTempDir()
        self.old_path = ""
        self.web_server_thread = None
        self.create_controls()
        self.create_menus()
        self.set_event_bindings()
        self.preview_type=PREVIEW_SPECIMEN
        self._read_font_dir()
        
    def set_event_bindings(self):
        
        self.Bind(wx.EVT_CLOSE, self.on_close)

        self.fontDirPanel.Bind(EVT_FONTDIR_CHANGEDIR,      self.on_change_dir)
        self.fontDirPanel.Bind(EVT_FONTDIR_SELECTEDFILE,   self.on_font_list_double_click)
        self.fontDirPanel.Bind(EVT_FONTDIR_TREESELCHANGED, self.on_tree_sel_changed)

        #self.fontInfoPanel.Bind(EVT_FONTINFO_CHANGESIZE,   self.on_font_size_changed)
        #self.fontInfoPanel.Bind(EVT_FONTINFO_SELECTEDFILE, self.on_font_info_text_changed)

        self.fontListBox.Bind(EVT_VFONTLIST,               self.on_vfontlist)

    def create_controls(self):
        mainPanel = wx.Panel(self)
        self.statusbar = wx.StatusBar(self,-1)
        self.statusbar.SetFieldsCount(5)
        self.statusbar.SetStatusWidths([-3,-4,-1,-1,-1])

        mainSplitter = wx.SplitterWindow(mainPanel)
        self.fontDirPanel = FontDirectoryPanel(mainSplitter)
        mainSplitPanel = wx.Panel(mainSplitter)


        self.panelNotebook = wx.Panel(mainSplitPanel)
        self.scrollBmpContainer = ScrolledBitmapContainer(self.panelNotebook, -1)        
        self.pnlVFontList = wx.Panel(self.panelNotebook,-1)
        self.fontListBox = VFontListBox(self.pnlVFontList,-1, style=wx.LB_MULTIPLE)
        pnl_s = wx.BoxSizer()
        pnl_s.Add(self.fontListBox,1,wx.EXPAND)
        self.pnlVFontList.SetSizer(pnl_s)
        self.pnlVFontList.Hide()
        
        nbs = wx.BoxSizer(wx.VERTICAL)
        nbs.Add(wx.StaticText(mainSplitPanel,-1,label="Preview font"),0, wx.EXPAND | wx.BOTTOM, border=5)
        nbs.Add(self.scrollBmpContainer,1,wx.EXPAND | wx.ALL)
        nbs.Add(self.pnlVFontList,1,wx.EXPAND | wx.ALL)
        self.panelNotebook.SetSizer(nbs)

        mainPanelSizer = wx.BoxSizer(wx.VERTICAL)
        
        #self.fontInfoPanel = FontInfoPanel(mainSplitPanel)

        #mainPanelSizer.Add(self.fontInfoPanel,  0, flag=wx.EXPAND | wx.ALL , border=5)
        mainPanelSizer.Add(self.panelNotebook,       1, flag=wx.EXPAND | wx.ALL, border=5)
        mainSplitPanel.SetSizer(mainPanelSizer)
        mainSplitter.SplitVertically(self.fontDirPanel,mainSplitPanel)
        mainSplitter.SetMinimumPaneSize(500)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(mainSplitter,  1, flag=wx.EXPAND | wx.ALL, border=5)
        mainPanel.SetSizer(sizer)

        self.SetStatusBar(self.statusbar)
        self.SetStatusBarPane(0)
        
    def create_menus(self):
        self.toolbar = self.CreateToolBar(wx.TB_HORIZONTAL
            | wx.TB_TEXT
        )
        self.createToplevelToolbar()
        self.createFontListBoxContextMenu()

    def createToplevelToolbar(self):
        icons = []
        for i in toolbarIcons:
            bmp = wx.Bitmap("./pics/"+i,wx.BITMAP_TYPE_PNG)
            icons.append(bmp)

        tsize = (48,48)
        open_bmp  = icons[0]
        copy_bmp  = icons[1]
        exit_bmp = icons[8]
        print_bmp = icons[9]
        about_bmp = icons[7]
        sample_bmp = icons[3]
        spec_bmp = icons[4]
        list_bmp = icons[5]
        pref_bmp = icons[6]

        self.toolbar.AddTool(10, "Open", open_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "Open", "Open a Directory", None)
        self.toolbar.AddTool(20, "Copy", copy_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "Copy", "Copy demo to bitmap", None)
        self.toolbar.AddTool(21, "Latex", print_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "Export to Latex", "Generate a fontbook in LaTeX format", None)
        #self.toolbar.AddRadioTool(40,"Simple", sample_bmp, wx.NullBitmap,"Simple text", "Set preview as a simple text")       
        self.toolbar.AddRadioTool(50,"Specimen", spec_bmp , wx.NullBitmap,"Specimen", "Set preview as a specimen sheet")      
        self.toolbar.AddRadioTool(55,"Fontlist", list_bmp , wx.NullBitmap,"Font List", "Show a list of fonts contained in directory")      
        self.toolbar.AddStretchableSpace()
        self.toolbar.AddTool(61, "Preferences", pref_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "About", "About this app", None)
        self.toolbar.AddTool(60, "About", about_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "About", "About this app", None)
        self.toolbar.AddTool(70, "Exit", exit_bmp, wx.NullBitmap, wx.ITEM_NORMAL, "Exit", "Exit app", None)
        self.bindToolbarEvents()
        self.toolbar.Realize()

    def bindToolbarEvents(self):
        self.Bind(wx.EVT_TOOL, self.on_set_directory, id=10)
        self.Bind(wx.EVT_TOOL, self.on_print, id=21)
        self.Bind(wx.EVT_TOOL, self.on_file_exit, id=70)
        self.Bind(wx.EVT_TOOL, self.set_preview_type_simple,      id=40)
        self.Bind(wx.EVT_TOOL, self.set_preview_type_specimen,  id=50)
        self.Bind(wx.EVT_TOOL, self.set_preview_type_list,  id=55)
        self.Bind(wx.EVT_TOOL, self.on_help_about,  id=60)
        self.Bind(wx.EVT_TOOL, self.on_help_preferences,  id=61)

    def createFontListBoxContextMenu(self):
        self.menu = wx.Menu()
        i =  self.menu.Append(-1, "Check/Uncheck")
        self.Bind(wx.EVT_MENU, self.on_toggle_vfont_selection, i)       
        i =  self.menu.Append(-1, "Check all")
        self.Bind(wx.EVT_MENU, self.on_vfont_list_select_all, i)       
        i =  self.menu.Append(-1, "Uncheck all")
        self.Bind(wx.EVT_MENU, self.on_vfont_list_deselect_all, i)       
        i =  self.menu.Append(-1, "Invert checklist")
        self.Bind(wx.EVT_MENU, self.on_vfont_list_invert_selection, i)       
        self.menu.AppendSeparator()
        i = self.menu.Append(-1, "Make a new group from checked font")
        self.Bind(wx.EVT_MENU, self.on_make_new_group_from_checked_fonts, i)       
        i =  self.menu.Append(-1, "Add checked fonts to current group")
        self.Bind(wx.EVT_MENU, self.on_add_checked_fonts_to_current_group, i)       
        i =  self.menu.Append(-1, "Add checked fonts to another group")
        self.Bind(wx.EVT_MENU, self.on_add_checked_fonts_to_another_group, i)       
        self.menu.AppendSeparator()
        i =  self.menu.Append(-1, "Generate Latex Preview File")
        self.fontListBox.Bind(wx.EVT_CONTEXT_MENU, self.showPopupMenu)

    def on_make_new_group_from_checked_fonts(self,evt):
        print ("makeNewGroupFromCheckedFont")

    def on_add_checked_fonts_to_current_group(self,evt):
        print ("makeNewGroupFromCheckedFont")

    def on_add_checked_fonts_to_another_group(self,evt):
        print ("addCheckedFontsToAnotherGroup")

    def showPopupMenu(self, evt):
        pos = evt.GetPosition()
        pos = self.fontListBox.ScreenToClient(pos)
        self.fontListBox.PopupMenu(self.menu, pos)

    def on_toggle_vfont_selection(self, evt):
        print ("on_toggle_vfont_selection")
        self.fontListBox.ToggleFontSelection()  

    def on_vfont_list_select_all(self, evt):
        print ("on_vfont_list_select_all")
        self.fontListBox.SelectAllFont()      

    def on_vfont_list_deselect_all(self, evt):
        print ("on_togglon_vfont_list_deselect_alle_vfont_selection")
        self.fontListBox.UnSelectAllFont()

    def on_vfont_list_invert_selection(self, evt):
        print ("on_vfont_list_invert_selection")
        self.fontListBox.InvertSelection()
              

    def _update_font_preview (self, sFile = None):     
        try:
            s = self.fontDirPanel.GetStringSelection()
            if sFile != None:
                s = sFile
            self.scrollBmpContainer.file = s
            print ("Selected: ",s)
            info =None
            if self.preview_type == PREVIEW_SPECIMEN:
                info = self.scrollBmpContainer.specimen_sheet()
            elif self.preview_type == PREVIEW_SIMPLE:
                info = self.scrollBmpContainer.create_bitmap()
            else:
                info = self.scrollBmpContainer.get_font_info()
            if info != None:
                (family, style) = info
                self.fontListBox.SelectItemByFont(family, style)
                #self.fontInfoPanel.font_name.SetLabel(family)
                #self.fontInfoPanel.font_style.SetLabel(style)
                if sFile != None: 
                    self.statusbar.SetStatusText(sFile,2)
                self.statusbar.SetStatusText(family,3)
                self.statusbar.SetStatusText(style,4)
            if self.preview_type == PREVIEW_SPECIMEN or self.preview_type == PREVIEW_SIMPLE:
                self.scrollBmpContainer.SetFocusIgnoringChildren()
        except Exception as e:
            print (e)            

    def on_tree_sel_changed(self,evt):
        filename = evt.filename
        self.fontDirPanel.list_box_font.SetStringSelection(filename)
        self._update_font_preview()
        #self.scrollBmpContainer.file = filename
        #self._update_font_preview(filename)

    def on_font_list_double_click(self, evt):
        filename = evt.filename
        self._update_font_preview(filename)

    def set_preview_type_simple (self, evt):
        print ("Set sipmple")
        self.preview_type = PREVIEW_SIMPLE
        try:
            self.scrollBmpContainer.Show()
            self.pnlVFontList.Hide()
            self.panelNotebook.GetSizer().Layout()
            self.panelNotebook.Layout()
            self._update_font_preview()
        except:
            pass        


    def set_preview_type_list (self, evt):
        print ("Set list")
        self.preview_type = PREVIEW_LIST
        try:
            self.scrollBmpContainer.Hide()
            self.pnlVFontList.Show()
            self.panelNotebook.GetSizer().Layout()
            self.panelNotebook.Layout()
            self._update_font_preview()
        except:
            pass        

    def set_preview_type_specimen (self, evt):
        print ("Set spec")
        self.preview_type = PREVIEW_SPECIMEN
        try:
            self.scrollBmpContainer.Show()
            self.pnlVFontList.Hide()
            self.panelNotebook.GetSizer().Layout()
            self.panelNotebook.Layout()
            self._update_font_preview()
        except:
            pass      

    def _force_vfontlist_update(self):  
        try:
            self.pnlVFontList.Hide()
            self.panelNotebook.Layout()
            wx.Yield()
            self.fontListBox.SetScrollPos(wx.VERTICAL,1,True)
            self.fontListBox.Update()
            self.panelNotebook.GetSizer().Layout()
            self.panelNotebook.Layout()
            self._update_font_preview()
            self.pnlVFontList.Show()
            self.panelNotebook.Layout()
        except Exception as exc:
            print ("Error _force_vfontlist_update ", exc)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

    def _read_font_dir(self):
        self.fontDirPanel.ReadFontDir()
        try:
            self.fontListBox.SetFontPath (self.path)
            if self.preview_type == PREVIEW_LIST:
                self._force_vfontlist_update()                
                print ("_read_font_dir list done")
        except Exception as exc:
            print ("Error _read_font_dir ", exc)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


    def on_font_info_text_changed(self, evt):
        try:
            print ("on_font_info_text_changed")
            self.scrollBmpContainer.sample_text = Preferences.SPECIMEN_TEXT
            self._update_font_preview()
            self.scrollBmpContainer.SetFocusIgnoringChildren()
        except Exception as e:
            print (e)


    def on_font_size_changed(self, evt):
        try:
            print ("on_font_size_changed")
            newSize = Preferences.SPECIMEN_FONT_SIZE
            self.scrollBmpContainer.sample_text = Preferences.SPECIMEN_TEXT
            self.scrollBmpContainer.size = Preferences.SPECIMEN_FONT_SIZE
            print ("new size ",newSize)
            self._update_font_preview()
            print ("preview updated")
            self.scrollBmpContainer.SetFocusIgnoringChildren()
            print ("bmp focused")
            try:
                self.fontListBox.SetPreviewSize (Preferences.SINGLE_LINE_FONT_SIZE)
                self.fontListBox.Update()
                self.panelNotebook.Layout()
                
                print ("list updated")
            except Exception as exc:
                print ("Error _read_font_dir ", exc)
            
        except Exception as e:
            print (e)

    def on_vfontlist(self, evt):
        self.statusbar.SetStatusText(evt.file,2)
        self.statusbar.SetStatusText(evt.family,3)
        self.statusbar.SetStatusText(evt.style,4)


    def _change_font_path(self, path):
        print ("Changing path to ",path)
        self.path = path
        self.fontDirPanel.list_directory.SetPath(path)
        self.fontListBox.SetFontPath(path)
        self.statusbar.SetStatusText(path,1)
        self._read_font_dir()

    def on_change_dir(self, evt):
        path = evt.path
        wx.YieldIfNeeded()
        self.scrollBmpContainer.path = path
        self._change_font_path( path ) 


    def on_set_directory(self,evt):
        #dlg = wx.DirDialog(self, "Choose font directory",'.')
        dlg = ImportFontFromDirectoryDialog(self)
        try:
            if self.path != "":
                dlg.SetPath(self.path)
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            data = dlg.GetValue()
            print (data)
            #self._change_font_path( dlg.GetPath() ) 
            self._change_font_path( data["path"] ) 
        except Exception:
            wx.LogError('Failed to open directory!')
            raise
        finally:
            dlg.Destroy()
        print ("on_set_directory")

    def on_close(self, evt):
        self.Destroy()


    def on_file_exit(self,evt):
        print ("on_file_exit")

    def on_help_about(self,evt):
        print ("on_help_about")


    def on_help_preferences(self,evt):
        print ("on_help_preferences")
        dlg = PreferencesDialog(self, title="Preferences", style= wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        res = dlg.ShowModal()
        if res == wx.ID_OK:
            data = dlg.GetValue()
            print (data)
            self.on_font_size_changed(evt)
            #self.fontListBox.SetPreviewSize(Preferences.SINGLE_LINE_FONT_SIZE)
            #self._force_vfontlist_update()                
        dlg.Destroy()

    def on_print(self, evt):
        with wx.FileDialog(self, "Generate LaTeX fontbook", wildcard="tex files (*.tex)|*.tex",
                        style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # save the current contents in the file
            pathname = fileDialog.GetPath()
            try:
                print ("saving latex file to ", pathname)
                self.fontListBox.PrintSpecimensToLatex(pathname)
            except IOError:
                wx.LogError("Cannot save current data in file '%s'." % pathname)        

class FontPreviewerApp(wx.App):
    def OnInit(self):
        FontPreviewerFrame(None,title="Font Viewer",size=(1000,700)).Show()
        return True
        

if __name__ == '__main__':
    app = FontPreviewerApp(False)
    app.MainLoop()                
