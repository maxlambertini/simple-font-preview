import wx
import wx.lib.scrolledpanel as scrolled
import wx.html2
from PIL import Image, ImageDraw, ImageFont
import glob, os, sys
from controls.Fonts import FontData
from controls.VFontControls import *



class SimfopreFrame(wx.Frame):
    def __init__ (self, *a, **k):
        wx.Frame.__init__(self,*a,**k)
        self.pnl = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.dir = wx.TextCtrl(self.pnl,-1)
        self.txtSize = wx.TextCtrl(self.pnl,-1, size=(100,-1))
        self.btn = wx.Button(self.pnl,-1,label="Change Dir")
        self.btnSize = wx.Button(self.pnl,-1,label="Change Size")
        sizer2.Add(self.dir, 1, wx.EXPAND | wx.ALL, border=10)
        sizer2.Add(self.btn, 0, wx.EXPAND | wx.ALL, border=10)
        sizer2.Add(self.txtSize, 0, wx.EXPAND | wx.ALL, border=10)
        sizer2.Add(self.btnSize, 0, wx.EXPAND | wx.ALL, border=10)
        self.FontList = VFontListBox(self.pnl)
        self.FontList.SetFontPath("K:/DVD/01/Fonts/www.fontsupply.com/fonts")
        sizer.Add(sizer2,       0, wx.EXPAND)
        sizer.Add(self.FontList,1, wx.EXPAND | wx.ALL, border=10 )
        self.pnl.SetSizer(sizer)
        self.Bind(wx.EVT_BUTTON, self.change_dir, self.btn)
        self.Bind(wx.EVT_BUTTON, self.change_size, self.btnSize)

    def change_dir (self,evt):
        self.FontList.SetItemCount(0)
        new_path = self.dir.GetValue()
        self.FontList.SetFontPath(new_path)

    def change_size (self,evt):
        new_size = int(self.txtSize.GetValue())
        self.FontList.SetPreviewSize(new_size)


class SimfopreApp(wx.App):
    def OnInit(self):
        SimfopreFrame(None,title="Font Viewer",size=(800,600)).Show()
        return True
        

if __name__ == '__main__':
    #pass
    app = SimfopreApp(False)
    app.MainLoop()                
