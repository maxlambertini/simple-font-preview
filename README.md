# Simple Font Previewer

This is a simple font previewer.

It was made to help the user preview the fonts contained into a directory without installing them. Currently it is pre-alpha quality: 
more of a proof of concept than a full-fledged program. 

As per today, you can 

* Select a directory and scan its font files (T1, TTF, OTF)
* Generate a specimen and copy it into the clipboard (in bitmap format, caveat emptor)
* Browse a list previewing the fonts contained in the directory. 
* Change specimen text settings and list preview settings.

### Requirements

You need a working installation of python 3 (from 3.5 onwards will do) and the following python libraries installed

~~~
wxpython >= 4.0
fonttools >= 3.40
cachetools >= 3.1
pillow >= 5.1
~~~

### Simple setup

~~~
git clone https://gitlab.com/maxlambertini/simple-font-preview
cd simple-font-preview
pip3 install -r requirements.txt
./fontpreviewer.py
~~~

### License / Disclaimer

You can test this software, but 

1. do it at your own risk. I will be held responsible of nothing happening on your system, should you ever try and run this viewer. 
2. I retain all rights on this stuff.